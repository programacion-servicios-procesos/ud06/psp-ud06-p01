import java.io.*;
import java.net.Socket;

public class TcpClient {
    private static final String CLIENT_NAME = "localhost";
    private static final int PORT = 1234;

    public static void main(String[] args) {
        try {
            int input;
            final File file = new File("Ex4-TransferenciaRemota/fileTransfer.txt");
            Socket socket = new Socket(CLIENT_NAME, PORT);

            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(socket.getOutputStream());
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            dataOutputStream.writeUTF(file.getName());

            byte[] byteArray = new byte[8192];

            while ((input = bufferedInputStream.read(byteArray)) != -1){
                bufferedOutputStream.write(byteArray,0, input);
            }
            bufferedInputStream.close();
            bufferedOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
