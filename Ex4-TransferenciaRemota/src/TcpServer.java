import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class TcpServer {
    private static final int PORT = 1234;

    public static void main(String[] args) {
        try {
            int input;
            ServerSocket serverSocket = new ServerSocket(PORT);

            while (true) {
                Socket socket = serverSocket.accept();

                byte[] receivedData = new byte[1024];
                BufferedInputStream bufferedInputStream = new BufferedInputStream(socket.getInputStream());
                DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream("Ex4-TransferenciaRemota/fileTransferTCP.txt"));

                while ((input = bufferedInputStream.read(receivedData)) != -1){
                    bufferedOutputStream.write(receivedData,0, input);
                }
                bufferedOutputStream.close();
                dataInputStream.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
