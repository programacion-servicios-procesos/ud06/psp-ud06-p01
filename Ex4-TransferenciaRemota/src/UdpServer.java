import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.ByteBuffer;
import java.util.zip.CRC32;

public class UdpServer {
    private static final int PORT = 1234;

    public static void main(String[] args) {
        try {
            DatagramSocket sk = new DatagramSocket(PORT);

            File targetFile = new File("Ex4-TransferenciaRemota/fileTransferUDP.txt");
            FileOutputStream fileOutputStream = new FileOutputStream(targetFile);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
//            PrintStream printStream = new PrintStream(fileOutputStream);

            byte[] data = new byte[1000];
            DatagramPacket pkt = new DatagramPacket(data, data.length);
            ByteBuffer b = ByteBuffer.wrap(data);
//            CRC32 crc = new CRC32();

            while(true) {
                pkt.setLength(data.length);
                sk.receive(pkt);
                b.rewind();

                bufferedOutputStream.write(data);
                System.out.println(data);

                bufferedOutputStream.close();
                fileOutputStream.close();
                break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
