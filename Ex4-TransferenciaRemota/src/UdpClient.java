import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.zip.CRC32;

public class UdpClient {
    private static final String CLIENT_NAME = "localhost";
    private static final int PORT = 1234;

    public static void main(String[] args) {
        try {
            DatagramSocket datagramSocket = new DatagramSocket();

            byte[] dataBytes = new byte[1000];
            ByteBuffer byteBuffer = ByteBuffer.wrap(dataBytes);

            File file = new File("Ex4-TransferenciaRemota/fileTransfer.txt");
            FileInputStream fileInputStream = new FileInputStream(file);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);

            DatagramPacket datagramPacket;

            CRC32 crc = new CRC32();

            while (true) {
                byteBuffer.clear();
                byteBuffer.putLong(0);
                int bytesRead = bufferedInputStream.read(dataBytes, 8, dataBytes.length - 8);
                if (bytesRead == -1) {
                    break;
                }
                crc.reset();
                crc.update(dataBytes, 8, dataBytes.length - 8);
                long checkSum = crc.getValue();
                byteBuffer.rewind();
                byteBuffer.putLong(checkSum);
                datagramPacket = new DatagramPacket(dataBytes, 1000, InetAddress.getByName(CLIENT_NAME), PORT);
                datagramSocket.send(datagramPacket);
            }
            bufferedInputStream.close();
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
