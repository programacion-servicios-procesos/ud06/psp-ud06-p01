import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class TcpClient {
    private static final int PORT = 9090;
    public static final String SERVER_NAME = "localhost";

    public static void main(String[] args) {
        try {
            Socket socket = new Socket(SERVER_NAME, PORT);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            System.out.println(bufferedReader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
