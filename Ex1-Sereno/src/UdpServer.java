import java.io.IOException;
import java.net.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class UdpServer {
    private static final int PORT = 9090;
    public static final String MACHINE = "localhost";

    public static void main(String[] args) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        try {
            InetSocketAddress socketAddress = new InetSocketAddress(MACHINE, PORT);
            DatagramSocket datagramSocket = new DatagramSocket(socketAddress);

            while (true) {
                byte[] message = new byte[40];
                DatagramPacket datagramPacket = new DatagramPacket(message, message.length);
                datagramSocket.receive(datagramPacket);

                InetAddress senderAddr = datagramPacket.getAddress();
                int senderPort = datagramPacket.getPort();

                String messageSent = "BIENVENIDO AL SERVIDOR SERENO, LA FECHA Y LA HORA ACTUALES SON: " + dateTimeFormatter.format(LocalDateTime.now());
                byte[] messageResponse = messageSent.getBytes();

                DatagramPacket datagramPacketBack = new DatagramPacket(messageResponse, messageResponse.length, senderAddr, senderPort);
                datagramSocket.send(datagramPacketBack);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
