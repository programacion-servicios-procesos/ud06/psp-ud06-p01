import java.io.IOException;
import java.net.*;

public class UdpClient {
    private static final int PORT = 9090;
    public static final String CLIENT_NAME = "localhost";

    public static void main(String[] args) {
        try {
            DatagramSocket datagramSocket = new DatagramSocket();

            String message = "WHAT TIME IS IT";
            DatagramPacket datagramPacket = new DatagramPacket(
                    message.getBytes(),
                    message.getBytes().length,
                    InetAddress.getByName(CLIENT_NAME),
                    PORT
            );
            datagramSocket.send(datagramPacket);

            byte[] response = new byte[100];
            datagramPacket = new DatagramPacket(response, response.length);
            datagramSocket.receive(datagramPacket);
            System.out.println(new String(response, 0, datagramPacket.getLength()));

            datagramSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
