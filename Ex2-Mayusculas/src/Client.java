import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private static final int PORT = 9090;
    public static final String CLIENT_NAME = "localhost";

    public static void main(String[] args) {

        try {
            String quitCommand;
            Scanner scanner = new Scanner(System.in);
            Socket socket = new Socket(CLIENT_NAME, PORT);
            PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            do {
                System.out.println(bufferedReader.readLine());
                quitCommand = scanner.nextLine();
                printWriter.println(quitCommand);
            } while (!quitCommand.equalsIgnoreCase("Quit"));
            System.out.println("Goodbye!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
