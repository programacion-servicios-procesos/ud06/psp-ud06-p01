import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.ServerSocket;

public class Server {
    private static final int PORT = 9090;

    public static void main(String[] args) {
        int clientCount = 1;

        try {
            ServerSocket serverSocket = new ServerSocket(PORT);
            File file = new File("Ex2-Mayusculas/log.txt");

            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fileWriter = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write("ClientNumber" + "\t" + "LocalPort" + "\t" + "ClientIP" + "\t\t\t" + "ClientPort" + "\t" + "NumberOfCapitalizations" + "\n");
            bufferedWriter.flush();

            while (true) {
                Thread serverThread = new Thread(new ThreadServer(serverSocket.accept(), fileWriter), "Client " + clientCount);
                serverThread.start();
                clientCount++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
