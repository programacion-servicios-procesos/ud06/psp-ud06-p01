import java.io.*;
import java.net.Socket;

public class ThreadServer implements Runnable {
    private final Socket socket;
    private FileWriter fileWriter;

    public ThreadServer(Socket socket, FileWriter fileWriter) {
        this.socket = socket;
        this.fileWriter = fileWriter;
    }

    @Override
    public void run() {
        try {
            String quitCommand;

            PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            printWriter.println("Hello! Write a text and I will return it capitalized (Type 'Quit' to exit)");

            do {
                quitCommand = bufferedReader.readLine();
                printWriter.println(quitCommand.toUpperCase());
                bufferedWriter.write(Thread.currentThread().getName() + "\t\t" + socket.getLocalPort() + "\t\t" + socket.getRemoteSocketAddress() + "\t\t" + socket.getPort() + "\t" + quitCommand.length() + "\n");
                bufferedWriter.flush();
            } while (!quitCommand.equalsIgnoreCase("Quit"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
