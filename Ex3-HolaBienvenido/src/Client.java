import java.io.IOException;
import java.net.*;
import java.util.Scanner;

public class Client {
    public static final int PORT = 1234;
    public static final String CLIENT_NAME = "localhost";

    public static void main(String[] args) {

        try {
            String message;
            Scanner scanner = new Scanner(System.in);
            DatagramSocket datagramSocket = new DatagramSocket();
            System.out.print("¡Saluda! ");
            message = scanner.nextLine();

            DatagramPacket datagramPacket = new DatagramPacket(message.getBytes(),
                    message.getBytes().length,
                    InetAddress.getByName(CLIENT_NAME),PORT);

            datagramSocket.send(datagramPacket);

            byte[] responseMessage = new byte[40];
            datagramPacket = new DatagramPacket(responseMessage, responseMessage.length);
            datagramSocket.receive(datagramPacket);
            System.out.println(new String(responseMessage, 0, datagramPacket.getLength()));
            datagramSocket.close();

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
