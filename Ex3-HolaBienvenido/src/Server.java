import java.io.IOException;
import java.net.*;
import java.util.concurrent.ThreadLocalRandom;

public class Server {
    public static final int PORT = 1234;
    public static final String CLIENT_NAME = "localhost";

    public static void main(String[] args) {
        try {
            String[] welcomeSentences = {"Yieeee", "¡Hombre Arturo!", "¿Que tal?", "¡Cuanto tiempo!"};
            InetSocketAddress socketAddress = new InetSocketAddress(CLIENT_NAME, PORT);
            DatagramSocket datagramSocket = new DatagramSocket(socketAddress);

            while (true) {
                byte[] message = new byte[40];

                DatagramPacket datagramPacket = new DatagramPacket(message, message.length);
                datagramSocket.receive(datagramPacket);

                InetAddress clientAddress = datagramPacket.getAddress();
                int senderPort = datagramPacket.getPort();

                String messageRecieved = new String(message, 0, datagramPacket.getLength());

                if (messageRecieved.toLowerCase().contains("hola") || messageRecieved.toLowerCase().contains("¡hola!")) {
                    String defaultReply = welcomeSentences[ThreadLocalRandom.current().nextInt(0, welcomeSentences.length)] + ", como estás?";
                    byte[] messageResponse = defaultReply.getBytes();
                    DatagramPacket datagramPacketBack = new DatagramPacket(messageResponse, messageResponse.length, clientAddress, senderPort);
                    datagramSocket.send(datagramPacketBack);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
